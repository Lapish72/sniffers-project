﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FileSniffer.Controllers
{
    [Serializable]
    public class FileController
    {
        #region Fields

        private Dictionary<string, string> _foundFiles = new Dictionary<string, string>();

        private const string BaseName = "base.snf";

        #endregion

        #region Properties

        /// <summary>
        /// Расширения файлов
        /// </summary>
        public string Pattern { get; set; } = "*.txt";

        public List<string> LastFiles { get; private set; } = new List<string>();

        #endregion

        #region Constructors

        public FileController()
        {
            Load();
        }

        #endregion

        #region Methods

        #region Public

        public async Task<List<string>> GetAsyncFiles(string path)
        {
            var files = new List<string>();

            try
            {
                files.AddRange(Directory.GetFiles(path, Pattern, SearchOption.TopDirectoryOnly));
                foreach (var directory in Directory.GetDirectories(path))
                    files.AddRange(await GetAsyncFiles(directory));
            }
            catch { }

            return files;
        }

        public List<string> ReadAllLines(string path)
        {
            try
            {
                return File.ReadAllLines(path).ToList();
            }
            catch
            {
                return new List<string>();
            }
        }

        public void AddFile(string path)
        {
            string fileHash = GetMD5Hash(path);
            if (!_foundFiles.ContainsKey(fileHash))
            {
                _foundFiles.Add(fileHash, path);
                LastFiles.Add(path);
            }
        }

        public void ClearCash()
        {
            LastFiles.Clear();
        }

        public void Save()
        {
            BinarySerialization.WriteToBinaryFile(BaseName, this);
        }

        #endregion

        #region Private

        private string GetMD5Hash(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return Encoding.Default.GetString(md5.ComputeHash(stream));
                }
            }
        }

        private void Load()
        {
            if (File.Exists(BaseName))
            {
                try
                {
                    var fileController =  BinarySerialization.ReadFromBinaryFile<FileController>(BaseName);
                    LastFiles = fileController.LastFiles;
                    _foundFiles = fileController._foundFiles;                    
                }
                catch
                {

                }
            }
        }

        #endregion

        #endregion
    }
}
